package com.nishchymenko.map;

import static org.junit.Assert.*;

import java.util.Map;
import org.junit.Test;

public class BinaryTreeMapTest {

  @Test
  public void putTest() {
    Map<Integer, String> map = new BinaryTreeMap<>();
    map.put(10, "a");
    map.put(1, "a");
    map.put(1, "b");
    map.put(2, "5");
    map.put(11, "a");
  }

}