package com.nishchymenko.map;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class BinaryTreeMap<K extends Comparable<K>, V> implements Map<K, V> {

  class Node implements Entry<K, V> {

    Node parent;
    Node right;
    Node left;
    K key;
    V value;

    @Override
    public K getKey() {
      return key;
    }

    @Override
    public V getValue() {
      return value;
    }

    @Override
    public V setValue(V value) {
      return this.value = value;
    }
  }

  private int size;
  private Node head;

  public BinaryTreeMap() {
    head = null;
    size = 0;
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public boolean isEmpty() {
    return size == 0;
  }

  @Override
  public boolean containsKey(Object key) {
    return false;
  }

  @Override
  public boolean containsValue(Object value) {
    return false;
  }

  @Override
  public V get(Object keyObject) {
    K key = (K) keyObject;
    if (head == null) {
      return null;
    }
    return get(key, head);
  }

  private V get(K key, Node parent) {
    if (parent.key == key) {
      return parent.value;
    }

    if (key.compareTo(parent.key) <= 0) {
      if (parent.left != null) {
        return get(key, parent.left);
      }
      return null;
    } else {
      if (parent.right != null) {
        return get(key, parent.right);
      }
      return null;
    }
  }

  @Override
  public V put(K key, V value) {
    Node node = new Node();
    node.value = value;
    node.key = key;

    if (head == null) {
      head = node;
      size++;

      return head.value;
    } else {
      return put(node, head);
    }
  }

  private V put(Node node, Node parent) {
    if (node.key.compareTo(parent.key) == 0) {
      parent.value = node.value;

      return parent.value;

    } else if (node.key.compareTo(parent.key) <= 0) {
      if (parent.left != null) {
        return put(node, parent.left);
      } else {
        parent.left = node;
        node.parent = parent;
        size++;

        return node.value;
      }
    } else {
      if (parent.right != null) {
        return put(node, parent.right);
      } else {
        parent.right = node;
        node.parent = parent;
        size++;

        return node.value;
      }

    }
  }

  @Override
  public V remove(Object keyObject) {
    K key = (K) keyObject;
    if (head == null) {
      return null;
    }
    return remove(key, head);
  }

  private V remove(K key, Node parent) {
    if (parent == null) {
      return null;
    }

    if (parent.key.compareTo(key) == 0) {
      delete(key, parent);
      return parent.value;
    } else if (key.compareTo(parent.key) <= 0) {
      return remove(key, parent.left);
    } else {
      return remove(key, parent.right);
    }
  }

  private void delete(K key, Node node) {
    if (node.right == null && node.left == null) {
      if (node.parent.left == node) {
        node.parent.left = null;
      } else {
        node.parent.right = null;
      }
      return;
    }

    if (node.right == null || node.left == null) {
      if (node.right != null) {
        if (node.parent.left == node) {
          node.parent.left = node.right;
        } else {
          node.parent.right = node.right;
        }
      }
      if (node.left != null) {
        if (node.parent.left == node) {
          node.parent.left = node.left;
        } else {
          node.parent.right = node.left;
        }
      }
      return;
    }

    Node minRight = findMin(node.right);
    delete(minRight.key, minRight);
    Node newNode = new Node();
    newNode.key = minRight.key;
    newNode.value = minRight.value;
    if (node.parent.left == node) {
      node.parent.left = newNode;
      node.parent = null;
      newNode.left = node.left;
      newNode.right = node.right;
      newNode.right.parent = newNode;
      newNode.left.parent = newNode;
      return;
    }
    if (node.parent.right == node) {
      node.parent.right = newNode;
      node.parent = null;
      newNode.left = node.left;
      newNode.right = node.right;
      newNode.right.parent = newNode;
      newNode.left.parent = newNode;
      return;
    }
  }

  private Node findMin(Node node) {
    if (node.left != null) {
      return findMin(node.left);
    } else {
      return node;
    }
  }

  @Override
  public void putAll(Map<? extends K, ? extends V> m) {

  }

  @Override
  public void clear() {
    head = null;
  }

  @Override
  public Set<K> keySet() {
    return null;
  }

  @Override
  public Collection<V> values() {
    return null;
  }

  @Override
  public Set<Entry<K, V>> entrySet() {
    return null;
  }
}
