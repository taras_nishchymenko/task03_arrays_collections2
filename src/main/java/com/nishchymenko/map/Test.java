package com.nishchymenko.map;

import java.util.Map;

public class Test {

  public static void main(String[] args) {
    Map<Integer, String> map = new BinaryTreeMap<>();
    map.put(10, "10");
    map.put(8, "8");
    map.put(7, "7");
    map.put(9, "9");
    map.put(12, "12");
    map.put(11, "11");
    map.put(13, "13");
    map.put(14, "14");

    System.out.println(map.remove(7));
    System.out.println(map.remove(13));
    System.out.println(map.remove(12));
  }

}
